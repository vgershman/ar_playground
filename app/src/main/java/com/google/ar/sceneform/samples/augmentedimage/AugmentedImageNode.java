/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.ar.sceneform.samples.augmentedimage;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.media.Image;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.ar.core.Anchor;
import com.google.ar.core.AugmentedImage;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.rendering.ViewSizer;

import java.util.concurrent.CompletableFuture;

/**
 * Node for rendering an augmented image. The image is framed by placing the virtual picture frame
 * at the corners of the augmented image trackable.
 */
@SuppressWarnings({"AndroidApiChecker"})
public class AugmentedImageNode extends AnchorNode {

    private static final String TAG = "AugmentedImageNode";

    // The augmented image represented by this node.
    private AugmentedImage image;

    // Models of the 4 corners.  We use completable futures here to simplify
    // the error handling and asynchronous loading.  The loading is started with the
    // first construction of an instance, and then used when the image is set.
    private static CompletableFuture<ViewRenderable> testView;
    //  private static CompletableFuture<ViewRenderable> testView2;
    private static CompletableFuture<ViewRenderable> testView3;

    public AugmentedImageNode(Context context) {
        // Upon construction, start loading the models for the corners of the frame.
        if (testView == null) {
            testView = ViewRenderable.builder().setHorizontalAlignment(ViewRenderable.HorizontalAlignment.CENTER)
                    .setVerticalAlignment(ViewRenderable.VerticalAlignment.CENTER).setView(context, R.layout.test_marker)
                    .build();
            testView.thenAccept(viewRenderable -> viewRenderable.getView().setOnClickListener(view -> {
                Toast.makeText(view.getContext(), "Test click top left", Toast.LENGTH_SHORT).show();
            }));
        }

        if (testView3 == null) {
            testView3 = ViewRenderable.builder().setHorizontalAlignment(ViewRenderable.HorizontalAlignment.CENTER)
                    .setVerticalAlignment(ViewRenderable.VerticalAlignment.CENTER).setView(context, R.layout.pager_test)
                    .build();
            testView3.thenAccept(viewRenderable -> setUpViewPager(viewRenderable.getView().findViewById(R.id.vp)));
        }
    }

    private void setUpViewPager(ViewPager2 vp) {
        vp.setAdapter(new GalleryAdapter());
        (vp.getAdapter()).notifyDataSetChanged();
    }

    class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.VH> {

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new VH(parent);
        }

        private int[] colors = new int[]{Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE};

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position) {
            if (position == 0) {
                holder.itemView.findViewById(R.id.ivImage).setAlpha(0);
            } else {
                holder.itemView.findViewById(R.id.ivImage).setAlpha(0.5f);
                ((ImageView) holder.itemView.findViewById(R.id.ivImage)).setColorFilter(colors[position - 1], PorterDuff.Mode.MULTIPLY);
            }
        }

        @Override
        public int getItemCount() {
            return 5;
        }

        public class VH extends RecyclerView.ViewHolder {

            public VH(@NonNull ViewGroup parent) {
                super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pager, parent, false));
            }
        }

    }

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Sceneform node tree is
     * created based on an Anchor created from the image. The corners are then positioned based on the
     * extents of the image. There is no need to worry about world coordinates since everything is
     * relative to the center of the image, which is the parent node of the corners.
     */
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    public void setImage(AugmentedImage image) {
        this.image = image;

        // If any of the models are not loaded, then recurse when all are loaded.
        if (!testView.isDone() || !testView3.isDone()) {
            CompletableFuture.allOf(testView, testView3)
                    .thenAccept((Void aVoid) -> setImage(image))
                    .exceptionally(
                            throwable -> {
                                Log.e(TAG, "Exception loading", throwable);
                                return null;
                            });
        }


        setAnchor(image.createAnchor(image.getCenterPose()));

        Vector3 localPosition = new Vector3();
        localPosition.set(-0.25f * image.getExtentX(), 0f, -0.25f * image.getExtentZ());
        Node cornerNode = new Node();
        cornerNode.setParent(this);
        cornerNode.setLocalPosition(localPosition);
        cornerNode.setLocalRotation(new Quaternion(45f, 0f, 0f, 30f));
        cornerNode.setRenderable(testView.getNow(null));

        localPosition.set(0, 0f, 0f);
        Node cornerNode2 = new Node();
        cornerNode2.setParent(this);
        cornerNode2.setLocalPosition(localPosition);
        cornerNode2.setLocalRotation(new Quaternion(-90f, 0f, 0f, 90f));
        cornerNode2.setRenderable(testView3.getNow(null));


    }

    public AugmentedImage getImage() {
        return image;
    }
}
